Wiregaurd Switch is just a handy bash script for quickly switching between configurations. Useful for those who have many configurations for mullvad vpn, for example, that they may need to quickly and thoughtlessly switch between. 

Usage: wg-switch                                 Lists contents of ~/.config/wg-switch/servers
      or   wg-switch <wiregaurd-configuration>   Switches Wiregaurd to specified configuration
      or   wg-switch off                         Disables Wiregaurd
      or   wg-switch help                        Shows this helpful text
